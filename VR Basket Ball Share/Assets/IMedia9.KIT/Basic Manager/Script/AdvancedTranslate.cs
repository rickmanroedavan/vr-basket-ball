﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar pergerakan dalam Unity yang terdiri dari Position, Rotation, & Scale.
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedTranslate : MonoBehaviour
{

    public enum CScriptFor { ThisGameObject, AnotherGameObject }

    public CScriptFor ScriptFor;
    public GameObject AnotherGameObject;

    public enum CVector3 { VectorForward, VectorBack, VectorLeft, VectorRight, VectorUp, VectorDown }
    public CVector3 ObjectVector;

    // Use this for initialization
    void Start()
    {

    }

    Vector3 GetVectorOption()
    {
        Vector3 result = Vector3.zero;
        switch (ObjectVector)
        {
            case CVector3.VectorForward:
                result = Vector3.forward;
                break;
            case CVector3.VectorBack:
                result = Vector3.back;
                break;
            case CVector3.VectorLeft:
                result = Vector3.left;
                break;
            case CVector3.VectorRight:
                result = Vector3.right;
                break;
            case CVector3.VectorUp:
                result = Vector3.up;
                break;
            case CVector3.VectorDown:
                result = Vector3.down;
                break;
        }
        return result;
    }


    // Update is called once per frame
    void Update()
    {
        if (ScriptFor == CScriptFor.ThisGameObject)
        {
            this.transform.Translate(GetVectorOption());
        }
        else if (ScriptFor == CScriptFor.AnotherGameObject)
        {
            AnotherGameObject.transform.Translate(GetVectorOption());
        }
    }
}
