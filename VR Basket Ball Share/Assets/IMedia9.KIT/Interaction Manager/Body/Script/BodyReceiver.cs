﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk manage item yang ditabrak
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace IMedia9
{
    public class BodyReceiver : MonoBehaviour
    {
        [System.Serializable]
        public class CHealthReceiver
        {
            public GlobalHealth HealthObject;

            [Header("Weapon Sender Settings")]
            public bool isDetectWeapon;
            public string[] WeaponSenderTagName;
            [Space(10)]

            [Header("Item Sender Settings")]
            public bool isDetectItem;
            public string[] ItemSenderTagName;
        }

        [System.Serializable]
        public class CScoreReceiver
        {
            public GlobalScore ScoreObject;

            [Header("Item Sender Settings")]
            public bool isDetectItem;
            public string[] ItemSenderTagName;
        }

        [System.Serializable]
        public class CTimeReceiver
        {
            public GlobalTime TimeObject;

            [Header("Item Sender Settings")]
            public bool isDetectItem;
            public string[] ItemSenderTagName;
        }

        [System.Serializable]
        public class CIntegerReceiver
        {
            public GlobalIntVar IntegerObject;

            [Header("Item Sender Settings")]
            public bool isDetectItem;
            public string[] ItemSenderTagName;
        }

        [System.Serializable]
        public class CFloatReceiver
        {
            public GlobalFloatVar FloatObject;

            [Header("Item Sender Settings")]
            public bool isDetectItem;
            public string[] ItemSenderTagName;
        }

        public enum COperator { AddValue, SetValue, SubValue }
        [Header("Body Receiver Settings")]
        public string BodyReceiverTagName;
        public Rigidbody RigidbodyObject;
        [Space(10)]

        [Header("Health Settings")]
        public bool usingHealthReceiver;
        public CHealthReceiver[] HealthReceiver;

        [Header("Score Settings")]
        public bool usingScoreReceiver;
        public CScoreReceiver[] ScoreReceiver;

        [Header("Time Settings")]
        public bool usingTimeReceiver;
        public CTimeReceiver[] TimeReceiver;

        [Header("Integer Settings")]
        public bool usingIntegerReceiver;
        public CIntegerReceiver[] IntegerReceiver;

        [Header("Float Settings")]
        public bool usingFloatReceiver;
        public CFloatReceiver[] FloatReceiver;

        [Header("Animation Settings")]
        public bool usingAnimation;
        public Animator TargetAnimator;

        [Header("GetHeal Settings")]
        public bool isGetHealEnabled;
        public string GetHealAnimationState;
        [Space(10)]
        public bool usingGetHealEvent;
        public UnityEvent GetHealEvent;

        [Header("GetHit Settings")]
        public bool isGetHitEnabled;
        public string GetHitAnimationState;
        [Space(10)]
        public bool usingGetHitEvent;
        public UnityEvent GetHitEvent;

        [Header("Shutdown Settings")]
        public bool isShutdownEnabled;
        public string ShutdownAnimationState;
        [Space(10)]
        public bool usingShutdownEvent;
        public UnityEvent ShutdownEvent;
        [Space(10)]
        public bool isSendingShutdownCommand;

        [Header("Debug Value")]
        public float DamageReceived;
        public float ItemReceived;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (usingHealthReceiver)
            {
                for (int i = 0; i < HealthReceiver.Length; i++)
                {
                    if (HealthReceiver[i].HealthObject.GetCurrentValue() <= 0)
                    {
                        ExecuteShutdownAnimation();
                    }
                }
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            //******************************************************************************* HEALTH RECEIVER
            if (usingHealthReceiver)
            {
                for (int i = 0; i < HealthReceiver.Length; i++)
                {
                    if (HealthReceiver[i].isDetectWeapon)
                    {
                        for (int j = 0; j < HealthReceiver[i].WeaponSenderTagName.Length; j++)
                        {
                            if (collider.tag == HealthReceiver[i].WeaponSenderTagName[j])
                            {
                                HealthTriggerWeapon(i, collider);
                            }
                        }
                    }
                    if (HealthReceiver[i].isDetectItem)
                    {
                        for (int j = 0; j < HealthReceiver[i].ItemSenderTagName.Length; j++)
                        {
                            if (collider.tag == HealthReceiver[i].ItemSenderTagName[j])
                            {
                                HealthTriggerItem(i, collider);
                            }
                        }
                    }
                }
            }
            //******************************************************************************* SCORE RECEIVER
            if (usingScoreReceiver)
            {
                for (int i = 0; i < ScoreReceiver.Length; i++)
                {
                    if (ScoreReceiver[i].isDetectItem)
                    {
                        for (int j = 0; j < ScoreReceiver[i].ItemSenderTagName.Length; j++)
                        {
                            if (collider.tag == ScoreReceiver[i].ItemSenderTagName[j])
                            {
                                ScoreTriggerItem(i, collider);
                            }
                        }
                    }
                }
            }
            //******************************************************************************* TIME RECEIVER
            if (usingTimeReceiver)
            {
                for (int i = 0; i < TimeReceiver.Length; i++)
                {
                    if (TimeReceiver[i].isDetectItem)
                    {
                        for (int j = 0; j < TimeReceiver[i].ItemSenderTagName.Length; j++)
                        {
                            if (collider.tag == TimeReceiver[i].ItemSenderTagName[j])
                            {
                                TimeTriggerItem(i, collider);
                            }
                        }
                    }
                }
            }
            //******************************************************************************* INTEGER RECEIVER
            if (usingIntegerReceiver)
            {
                for (int i = 0; i < IntegerReceiver.Length; i++)
                {
                    if (IntegerReceiver[i].isDetectItem)
                    {
                        for (int j = 0; j < IntegerReceiver[i].ItemSenderTagName.Length; j++)
                        {
                            if (collider.tag == IntegerReceiver[i].ItemSenderTagName[j])
                            {
                                IntegerTriggerItem(i, collider);
                            }
                        }
                    }
                }
            }
            //******************************************************************************* FLOAT RECEIVER
            if (usingFloatReceiver)
            {
                for (int i = 0; i < FloatReceiver.Length; i++)
                {
                    if (FloatReceiver[i].isDetectItem)
                    {
                        for (int j = 0; j < FloatReceiver[i].ItemSenderTagName.Length; j++)
                        {
                            if (collider.tag == FloatReceiver[i].ItemSenderTagName[j])
                            {
                                FloatTriggerItem(i, collider);
                            }
                        }
                    }
                }
            }
        }

        void HealthTriggerWeapon(int Index, Collider collider)
        {
            GameObject temp = GameObject.Find(collider.gameObject.name);

            if (temp.GetComponent<WeaponSender>() == null)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to attach an WeaponSender script?");
            }

            float temp_Damage = temp.GetComponent<WeaponSender>().DamageValue;
            bool temp_destroy = temp.GetComponent<WeaponSender>().DestroyAfterCollision;
            COperator temp_operator = (COperator)temp.GetComponent<WeaponSender>().DamageOperator;
            DamageReceived = temp_Damage;

            if (temp_operator == COperator.AddValue)
            {
                HealthReceiver[Index].HealthObject.AddToCurrentValue(DamageReceived);
                DamageReceived = 0;
                ExecuteGetHealAnimation();
            }
            if (temp_operator == COperator.SetValue)
            {
                HealthReceiver[Index].HealthObject.SetCurrentValue(DamageReceived);
                DamageReceived = 0;
            }
            if (temp_operator == COperator.SubValue)
            {
                HealthReceiver[Index].HealthObject.SubToCurrentValue(DamageReceived);
                DamageReceived = 0;
                ExecuteGetHitAnimation();
            }
            if (temp_destroy)
            {
                Destroy(temp);
            }
        }

        void HealthTriggerItem(int Index, Collider collider)
        {
            GameObject temp = GameObject.Find(collider.gameObject.name);

            if (temp.GetComponent<ItemSender>() == null)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to attach an ItemSender script?");
            }

            float temp_Value = temp.GetComponent<ItemSender>().ItemValue;
            bool temp_destroy = temp.GetComponent<ItemSender>().DestroyAfterCollision;
            COperator temp_operator = (COperator)temp.GetComponent<ItemSender>().ItemOperator;
            ItemReceived = temp_Value;

            if (temp_operator == COperator.AddValue)
            {
                HealthReceiver[Index].HealthObject.AddToCurrentValue(ItemReceived);
                ItemReceived = 0;
                ExecuteGetHealAnimation();
            }
            if (temp_operator == COperator.SetValue)
            {
                HealthReceiver[Index].HealthObject.SetCurrentValue(ItemReceived);
                ItemReceived = 0;
            }
            if (temp_operator == COperator.SubValue)
            {
                HealthReceiver[Index].HealthObject.SubToCurrentValue(ItemReceived);
                ItemReceived = 0;
                ExecuteGetHitAnimation();
            }
            if (temp_destroy)
            {
                Destroy(temp);
            }
        }

        void ScoreTriggerItem(int Index, Collider collider)
        {
            GameObject temp = GameObject.Find(collider.gameObject.name);

            if (temp.GetComponent<ItemSender>() == null)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to attach an ItemSender script?");
            }

            float temp_Value = temp.GetComponent<ItemSender>().ItemValue;
            bool temp_destroy = temp.GetComponent<ItemSender>().DestroyAfterCollision;
            COperator temp_operator = (COperator)temp.GetComponent<ItemSender>().ItemOperator;
            ItemReceived = temp_Value;

            if (temp_operator == COperator.AddValue)
            {
                ScoreReceiver[Index].ScoreObject.AddToCurrentValue(ItemReceived);
                ItemReceived = 0;
            }
            if (temp_operator == COperator.SetValue)
            {
                ScoreReceiver[Index].ScoreObject.SetCurrentValue(ItemReceived);
                ItemReceived = 0;
            }
            if (temp_operator == COperator.SubValue)
            {
                ScoreReceiver[Index].ScoreObject.SubToCurrentValue(ItemReceived);
                ItemReceived = 0;
            }
            if (temp_destroy)
            {
                Destroy(temp);
            }
        }

        void TimeTriggerItem(int Index, Collider collider)
        {
            GameObject temp = GameObject.Find(collider.gameObject.name);

            if (temp.GetComponent<ItemSender>() == null)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to attach an ItemSender script?");
            }

            float temp_Value = temp.GetComponent<ItemSender>().ItemValue;
            bool temp_destroy = temp.GetComponent<ItemSender>().DestroyAfterCollision;
            COperator temp_operator = (COperator)temp.GetComponent<ItemSender>().ItemOperator;
            ItemReceived = temp_Value;

            if (temp_operator == COperator.AddValue)
            {
                TimeReceiver[Index].TimeObject.AddToCurrentTime(Mathf.RoundToInt(ItemReceived));
                ItemReceived = 0;
                ExecuteGetHealAnimation();
            }
            if (temp_operator == COperator.SetValue)
            {
                TimeReceiver[Index].TimeObject.SetCurrentTime(Mathf.RoundToInt(ItemReceived));
                ItemReceived = 0;
            }
            if (temp_operator == COperator.SubValue)
            {
                TimeReceiver[Index].TimeObject.SubstractFromCurrentTime(Mathf.RoundToInt(ItemReceived));
                ItemReceived = 0;
                ExecuteGetHitAnimation();
            }
            if (temp_destroy)
            {
                Destroy(temp);
            }
        }

        void IntegerTriggerItem(int Index, Collider collider)
        {
            GameObject temp = GameObject.Find(collider.gameObject.name);

            if (temp.GetComponent<ItemSender>() == null)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to attach an ItemSender script?");
            }

            float temp_Value = temp.GetComponent<ItemSender>().ItemValue;
            bool temp_destroy = temp.GetComponent<ItemSender>().DestroyAfterCollision;
            COperator temp_operator = (COperator)temp.GetComponent<ItemSender>().ItemOperator;
            ItemReceived = temp_Value;

            if (temp_operator == COperator.AddValue)
            {
                IntegerReceiver[Index].IntegerObject.AddToCurrentValue(Mathf.RoundToInt(ItemReceived));
                ItemReceived = 0;
                ExecuteGetHealAnimation();
            }
            if (temp_operator == COperator.SetValue)
            {
                IntegerReceiver[Index].IntegerObject.SetCurrentValue(Mathf.RoundToInt(ItemReceived));
                ItemReceived = 0;
            }
            if (temp_operator == COperator.SubValue)
            {
                IntegerReceiver[Index].IntegerObject.SubstractFromCurrentValue(Mathf.RoundToInt(ItemReceived));
                ItemReceived = 0;
                ExecuteGetHitAnimation();
            }
            if (temp_destroy)
            {
                Destroy(temp);
            }
        }

        void FloatTriggerItem(int Index, Collider collider)
        {
            GameObject temp = GameObject.Find(collider.gameObject.name);

            if (temp.GetComponent<ItemSender>() == null)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to attach an ItemSender script?");
            }

            float temp_Value = temp.GetComponent<ItemSender>().ItemValue;
            bool temp_destroy = temp.GetComponent<ItemSender>().DestroyAfterCollision;
            COperator temp_operator = (COperator)temp.GetComponent<ItemSender>().ItemOperator;
            ItemReceived = temp_Value;

            if (temp_operator == COperator.AddValue)
            {
                FloatReceiver[Index].FloatObject.AddToCurrentValue(ItemReceived);
                ItemReceived = 0;
                ExecuteGetHealAnimation();
            }
            if (temp_operator == COperator.SetValue)
            {
                FloatReceiver[Index].FloatObject.SetCurrentValue(ItemReceived);
                ItemReceived = 0;
            }
            if (temp_operator == COperator.SubValue)
            {
                FloatReceiver[Index].FloatObject.SubToCurrentValue(ItemReceived);
                ItemReceived = 0;
                ExecuteGetHitAnimation();
            }
            if (temp_destroy)
            {
                Destroy(temp);
            }
        }



        public void ExecuteGetHealAnimation()
        {
            if (usingAnimation && isGetHealEnabled)
            {
                TargetAnimator.Play(GetHealAnimationState);
                
            }
            if (usingGetHealEvent)
            {
                GetHealEvent.Invoke();
            }
        }

        public void ExecuteGetHitAnimation()
        {
            if (usingAnimation && isGetHitEnabled)
            {
                if (usingHealthReceiver)
                {
                    for (int i = 0; i < HealthReceiver.Length; i++)
                    {
                        if (HealthReceiver[i].HealthObject.GetCurrentValue() > 0)
                        {
                            TargetAnimator.Play(GetHitAnimationState);
                        }
                    }
                }
            }
            if (usingGetHitEvent)
            {
                GetHitEvent.Invoke();
            }
        }

        public void ExecuteShutdownAnimation()
        {
            if (usingAnimation && isShutdownEnabled)
            {
                TargetAnimator.Play(ShutdownAnimationState);
            }
            if (usingShutdownEvent)
            {
                ShutdownEvent.Invoke();
            }
            if (isSendingShutdownCommand)
            {
                Debug.Log("Sending: Shutdown Animation Command");
                TargetAnimator.gameObject.SendMessage("Shutdown", true, SendMessageOptions.DontRequireReceiver);
                isSendingShutdownCommand = false;
            }
        }

    }

}