﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin camera dengan split screen display
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace IMedia9 {

    public class CameraSSD : MonoBehaviour {

        public enum CCameraType { IsCameraPrimary, IsCameraSecondary }

        [Header("Camera Settings")]
        public static CCameraType CameraType;
        public Camera CameraPrimary;
        public Camera CameraSecondary;

        [Header("Sun Settings")]
        public Light LightSun;
        public float LightSunPrimary;
        public float LightSunSecondary;

        [Header("Initialize Settings")]
        public UnityEvent CameraEvent;

        [Header("Trigger Settings")]
        public KeyCode KeyMultiDisplay;
        public KeyCode KeySplitCamera;
        public KeyCode KeyCameraPrimary;
        public KeyCode KeyCameraSecondary;

        [Header("Debug Value")]
        public static bool isMultiDisplay = false;

        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < Display.displays.Length; i++)
            {
                Display.displays[i].Activate();
            }

            CameraPrimary.targetDisplay = 0;
            CameraSecondary.targetDisplay = 0;

            CameraEvent.Invoke();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyMultiDisplay))
            {
                isMultiDisplay = !isMultiDisplay;
            }
            if (Input.GetKey(KeyCameraPrimary))
            {
                ActivateCameraPrimary();
            }
            if (Input.GetKey(KeyCameraSecondary))
            {
                ActivateCameraSecondary();
            }
            if (Input.GetKey(KeySplitCamera))
            {
                ActivateSplitCamera();
            }
        }

        public void ActivateCameraPrimary()
        {
            CameraType = CCameraType.IsCameraPrimary;

            CameraPrimary.rect = new Rect(0, 0, 1, 1);
            CameraPrimary.targetDisplay = 0;
            CameraPrimary.depth = 1;

            if (isMultiDisplay)
            {
                if (Display.displays.Length > 0 && Display.displays[1].active)
                {
                    CameraSecondary.targetDisplay = 1;
                    CameraSecondary.rect = new Rect(0, 0, 1, 1);
                }
            }

            CameraPrimary.gameObject.GetComponent<CameraMAP>().ActivateCamera(true);

            if (LightSun) LightSun.intensity = LightSunPrimary;
        }

        public void ActivateCameraSecondary()
        {
            CameraType = CCameraType.IsCameraSecondary;

            CameraSecondary.rect = new Rect(0, 0, 1, 1);
            CameraSecondary.targetDisplay = 0;
            CameraSecondary.depth = 1;

            if (isMultiDisplay)
            {
                if (Display.displays.Length > 1 && Display.displays[1].active)
                {
                    CameraPrimary.targetDisplay = 1;
                    CameraPrimary.rect = new Rect(0, 0, 1, 1);
                }
            }

            CameraSecondary.gameObject.GetComponent<CameraUFO>().ActivateCamera(true);

            if (LightSun) LightSun.intensity = LightSunSecondary;
        }

        public void ActivateSplitCamera()
        {
            CameraType = CCameraType.IsCameraPrimary;

            CameraPrimary.rect = new Rect(0, 0, 0.5f, 1);
            CameraSecondary.rect = new Rect(0.5f, 0, 1, 1);

            if (isMultiDisplay)
            {
                CameraPrimary.targetDisplay = 0;
                CameraSecondary.targetDisplay = 1;
            }
        }

        public static bool IsMultiDisplay()
        {
            return isMultiDisplay;
        }
    }
}
