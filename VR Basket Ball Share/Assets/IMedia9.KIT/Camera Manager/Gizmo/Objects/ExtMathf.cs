﻿using System;

namespace IMedia9
{
	public static class ExtMathf
	{
		public static float Squared(this float value)
		{
			return value * value;
		}
	}
}