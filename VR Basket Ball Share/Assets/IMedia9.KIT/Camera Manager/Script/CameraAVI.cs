﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera untuk pembuatan cutscene. 
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAVI : MonoBehaviour {


    public enum CameraType { ZoomIn, ZoomOut, MoveLeft, MoveRight, MoveUp, MoveDown }
    public enum RotationType { RotateLeft, RotateRight }
    public bool isEnabled;
    public GameObject TargetCamera;

    [Header("Straight Settings")]
    public bool isStraight;
    public CameraType cameraType = CameraType.ZoomIn;
    public float StraightSpeed = 0.5f;

    [Header("Rotation Settings")]
    public bool isRotation;
    public RotationType rotationType;
    public GameObject TargetObject;
    public float RotationSpeed = 0.5f;
    private Quaternion lookRotation;
    private Vector3 direction;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnabled)
        {
            if (isStraight)
            {
                if (cameraType == CameraType.ZoomIn)
                {
                    TargetCamera.transform.Translate(Vector3.forward * StraightSpeed * Time.deltaTime);
                }
                if (cameraType == CameraType.ZoomOut)
                {
                    TargetCamera.transform.Translate(Vector3.back * StraightSpeed * Time.deltaTime);
                }
                if (cameraType == CameraType.MoveLeft)
                {
                    TargetCamera.transform.Translate(Vector3.left * StraightSpeed * Time.deltaTime);
                }
                if (cameraType == CameraType.MoveRight)
                {
                    TargetCamera.transform.Translate(Vector3.right * StraightSpeed * Time.deltaTime);
                }
                if (cameraType == CameraType.MoveUp)
                {
                    TargetCamera.transform.Translate(Vector3.up * StraightSpeed * Time.deltaTime);
                }
                if (cameraType == CameraType.MoveDown)
                {
                    TargetCamera.transform.Translate(Vector3.down * StraightSpeed * Time.deltaTime);
                }
            }
            if (isRotation)
            {
                if (rotationType == RotationType.RotateLeft)
                {
                    TargetObject.transform.Rotate(0, RotationSpeed * Time.deltaTime, 0);
                    TargetCamera.transform.parent = TargetObject.transform;
                    TargetCamera.transform.LookAt(TargetObject.transform);
                }
                if (rotationType == RotationType.RotateRight)
                {
                    TargetObject.transform.Rotate(0, -RotationSpeed * Time.deltaTime, 0);
                    TargetCamera.transform.parent = TargetObject.transform;
                    TargetCamera.transform.LookAt(TargetObject.transform);
                }
            }
        }
	}
}
