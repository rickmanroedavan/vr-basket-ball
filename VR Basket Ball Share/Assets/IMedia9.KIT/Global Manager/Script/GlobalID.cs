﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menampung nilai global aplikasi
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class GlobalID : MonoBehaviour
    {

        [Header("Application Settings")]
        public string appName;
        public string appType;
        public string appDesc;
        public static string AppName;
        public static string AppType;
        public static string AppDesc;

        [Header("User Settings")]
        public string userName;
        public string userType;
        public string userDesc;
        public static string UserName;
        public static string UserType;
        public static string UserDesc;

        [Header("Caption Settings")]
        public bool usingCaption;
        public bool showAppName;
        public bool showAppType;
        public bool showAppDesc;
        public bool showUserName;
        public bool showUserType;
        public bool showUserDesc;
        public Text textCaption;

        [Header("Trigger Settings")]
        public KeyCode TriggerKey;
        bool isShow = false;

        // Use this for initialization
        void Awake()
        {
            AppName = appName;
            AppType = appType;
            AppDesc = appDesc;
            UserName = userName;
            UserType = userType;
            UserDesc = userDesc;
            InvokeRepeating("Syncronize", 1, 1);
        }

        void Syncronize()
        {
            string temp = "";

            appName = AppName;
            appType = AppType;
            appDesc = AppDesc;
            userName = UserName;
            userType = UserType;
            userDesc = UserDesc;

            if (usingCaption)
            {
                if (showAppName) temp = " :: " + AppName;
                if (showAppType) temp = temp + " :: " + AppType;
                if (showAppDesc) temp = temp + " :: " + AppDesc;
                if (showUserName) temp = temp + " :: " + UserName;
                if (showUserType) temp = temp + " :: " + UserType;
                if (showUserDesc) temp = temp + " :: " + UserDesc;
                textCaption.text = temp;
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (Input.GetKeyUp(TriggerKey))
            {
                isShow = !isShow;
            }
        }

        void OnGUI()
        {
            if (isShow)
            {
                GUI.Label(new Rect(10, 10, 500, 30), AppName);
                GUI.Label(new Rect(10, 30, 500, 30), AppType);
                GUI.Label(new Rect(10, 50, 500, 30), AppDesc);
                GUI.Label(new Rect(10, 70, 500, 30), UserName);
                GUI.Label(new Rect(10, 90, 500, 30), UserType);
                GUI.Label(new Rect(10, 110, 500, 30), UserDesc);
            }
        }
    }
}
