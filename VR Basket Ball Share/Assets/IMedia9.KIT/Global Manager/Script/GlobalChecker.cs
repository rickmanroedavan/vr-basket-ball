﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace IMedia9
{
    public class GlobalChecker : MonoBehaviour
    {
        public enum CConditionType { Greater, Equal, Less }

        [Space(10)]
        public bool isEnabled;

        [Header("Global Variable")]
        public GlobalVariable.CVariableType VariableType;
        public GlobalTime TimeVariables;
        public GlobalHealth HealthVariables;
        public GlobalScore ScoreVariables;
        public GlobalIntVar IntVariables;
        public GlobalFloatVar FloatVariables;
        public GlobalStringVar StringVariables;
        public GlobalBoolVar BoolVariables;

        [Header("Condition Settings")]
        public CConditionType ConditionType;
        public string ConditionValue;

        [Header("Action Settings")]
        public UnityEvent ConditionEvent;

        bool isInvoke = false;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!isInvoke && isEnabled)
            {
                if (VariableType == GlobalVariable.CVariableType.timeVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (TimeVariables.CurrentTime.ToString() == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Greater)
                    {
                        if (TimeVariables.CurrentTime >= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Less)
                    {
                        if (TimeVariables.CurrentTime <= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.healthVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (HealthVariables.CurrentValue.ToString() == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Greater)
                    {
                        if (HealthVariables.CurrentValue >= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Less)
                    {
                        if (HealthVariables.CurrentValue <= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.scoreVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (ScoreVariables.CurrentValue.ToString() == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Greater)
                    {
                        if (ScoreVariables.CurrentValue >= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Less)
                    {
                        if (ScoreVariables.CurrentValue <= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.intVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (IntVariables.CurrentValue.ToString() == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Greater)
                    {
                        if (IntVariables.CurrentValue >= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Less)
                    {
                        if (IntVariables.CurrentValue <= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.floatVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (FloatVariables.CurrentValue.ToString() == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Greater)
                    {
                        if (FloatVariables.CurrentValue >= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                    if (ConditionType == CConditionType.Less)
                    {
                        if (FloatVariables.CurrentValue <= int.Parse(ConditionValue))
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.boolVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (BoolVariables.CurrentValue.ToString() == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.stringVar)
                {
                    if (ConditionType == CConditionType.Equal)
                    {
                        if (StringVariables.CurrentValue == ConditionValue)
                        {
                            ConditionEvent.Invoke();
                            isInvoke = true;
                        }
                    }
                }
            }

        }
    }
}
