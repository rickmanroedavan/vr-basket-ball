﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk mengatur kondisi pause dan play
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalPlay : MonoBehaviour
    {

        public enum CGameType { TimeScaleMode, GameObjectMode }
        public enum CGameStatus { IsPlay, IsPause }

        [Space(10)]
        public CGameType GameType;
        public CGameStatus GameStatus;

        [Header("Play/Pause Settings")]
        public GameObject[] TargetObject;
        public AudioSource[] TargetAudio;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GameType == CGameType.TimeScaleMode)
            {
                if (GameStatus == CGameStatus.IsPlay)
                {
                    Time.timeScale = 1;
                }
                if (GameStatus == CGameStatus.IsPause)
                {
                    Time.timeScale = 0;
                }
            }
            if (GameType == CGameType.GameObjectMode)
            {
                if (GameStatus == CGameStatus.IsPlay)
                {
                    for (int i = 0; i < TargetObject.Length; i++)
                    {
                        TargetObject[i].SendMessage("SetPlay", true, SendMessageOptions.DontRequireReceiver);
                    }
                    for (int i = 0; i < TargetAudio.Length; i++)
                    {
                        TargetAudio[i].Pause();
                    }
                }
                if (GameStatus == CGameStatus.IsPause)
                {
                    for (int i = 0; i < TargetObject.Length; i++)
                    {
                        TargetObject[i].SendMessage("SetPause", true, SendMessageOptions.DontRequireReceiver);
                    }
                    for (int i = 0; i < TargetAudio.Length; i++)
                    {
                        TargetAudio[i].UnPause();
                    }
                }
            }
        }

        public void SetGamePlay()
        {
            GameStatus = CGameStatus.IsPlay;
        }

        public void SetGamePause()
        {
            GameStatus = CGameStatus.IsPause;
        }


    }
}
