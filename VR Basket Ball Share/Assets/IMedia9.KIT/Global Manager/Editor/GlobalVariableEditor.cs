﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Deklarasi Global Variable untuk 6 variabel standar
 **************************************************************************************************************/

using UnityEditor;
using UnityEngine;

namespace IMedia9
{
    [CustomEditor(typeof(GlobalVariable)), CanEditMultipleObjects]
    public class GlobalVariableEditor : Editor
    {

        public SerializedProperty
            enum_Status,
            timeVariables_prop,
            healthVariables_prop,
            scoreVariables_prop,
            intVariables_prop,
            floatVariables_prop,
            boolVariables_prop,
            stringVariables_prop
            ;

        void OnEnable()
        {
            // Setup the SerializedProperties
            enum_Status = serializedObject.FindProperty("VariableType");
            timeVariables_prop = serializedObject.FindProperty("TimeVariables");
            healthVariables_prop = serializedObject.FindProperty("HealthVariables");
            scoreVariables_prop = serializedObject.FindProperty("ScoreVariables");
            intVariables_prop = serializedObject.FindProperty("IntVariables");
            floatVariables_prop = serializedObject.FindProperty("FloatVariables");
            boolVariables_prop = serializedObject.FindProperty("BoolVariables");
            stringVariables_prop = serializedObject.FindProperty("StringVariables");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(enum_Status);

            GlobalVariable.CVariableType st = (GlobalVariable.CVariableType)enum_Status.enumValueIndex;

            switch (st)
            {
                case GlobalVariable.CVariableType.timeVar:
                    EditorGUILayout.PropertyField(timeVariables_prop, new GUIContent("TimeVariables"));
                    break;
                case GlobalVariable.CVariableType.healthVar:
                    EditorGUILayout.PropertyField(healthVariables_prop, new GUIContent("HealthVariables"));
                    break;
                case GlobalVariable.CVariableType.scoreVar:
                    EditorGUILayout.PropertyField(scoreVariables_prop, new GUIContent("ScoreVariables"));
                    break;
                case GlobalVariable.CVariableType.intVar:
                    EditorGUILayout.PropertyField(intVariables_prop, new GUIContent("IntVariables"));
                    break;
                case GlobalVariable.CVariableType.floatVar:
                    EditorGUILayout.PropertyField(floatVariables_prop, new GUIContent("FloatVariables"));
                    break;
                case GlobalVariable.CVariableType.stringVar:
                    EditorGUILayout.PropertyField(stringVariables_prop, new GUIContent("StringVariables"));
                    break;
                case GlobalVariable.CVariableType.boolVar:
                    EditorGUILayout.PropertyField(boolVariables_prop, new GUIContent("BoolVariables"));
                    break;
            }

            serializedObject.ApplyModifiedProperties();
        }

    }

}