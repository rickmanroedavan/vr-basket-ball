﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera ala game MOBA/DOTA multi karakter
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class CameraMMO : MonoBehaviour
    {
        public bool isEnabled;
        public Camera TargetCamera;
        public GameObject[] TargetObject;
        public float Delay = 0.1f;
        Vector3 targetDistance;
        Vector3 firstPosition;

        // Use this for initialization
        void Awake()
        {
            firstPosition = TargetCamera.transform.position;
        }

        void Start()
        {
            if (isEnabled)
            {
                Invoke("SetFirstPosition", Delay);
            }
        }

        void SetFirstPosition()
        {
            TargetCamera.transform.position = firstPosition;
            for (int i = 0; i < TargetObject.Length; i++)
            {
                if (TargetObject[i].activeSelf)
                {
                    targetDistance = TargetCamera.transform.position - TargetObject[i].transform.position;
                }
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (isEnabled)
            {
                for (int i = 0; i < TargetObject.Length; i++)
                {
                    if (TargetObject[i].activeSelf)
                    {
                        TargetCamera.transform.position = TargetObject[i].transform.position + targetDistance;
                    }
                }
            }
        }
    }
}
