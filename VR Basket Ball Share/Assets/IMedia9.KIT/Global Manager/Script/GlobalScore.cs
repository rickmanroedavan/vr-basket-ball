﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menampilkan nilai score
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalScore : MonoBehaviour
    {

        public float CurrentValue;

        [System.Serializable]
        public class CScoreRange
        {
            public string ScoreStatus;
            public float MinValue;
            public float MaxValue;
        }

        [Header("Score Settings")]
        public bool usingRange;
        public CScoreRange[] ScoreRange;

        [Header("Debug Value")]
        public string CurrentScoreStatus;

        int MinValue;

        void Start()
        {
        }

        void Update()
        {
            if (usingRange)
            {
                for (int i=0; i< ScoreRange.Length; i++)
                {
                    if (ScoreRange[i].MinValue <= CurrentValue && CurrentValue <= ScoreRange[i].MaxValue)
                    {
                        CurrentScoreStatus = ScoreRange[i].ScoreStatus;
                    }
                }
            }
        }

        public float GetCurrentValue()
        {
            return CurrentValue;
        }

        public string GetCurrentScoreStatus()
        {
            return CurrentScoreStatus;
        }

        public void SetCurrentValue(float aValue)
        {
            CurrentValue = aValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public void AddToCurrentValue(float aValue)
        {
            CurrentValue += aValue;
        }

        public void SubToCurrentValue(float aValue)
        {
            CurrentValue -= aValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public bool IsShutdown()
        {
            return CurrentValue <= 0;
        }

    }
}