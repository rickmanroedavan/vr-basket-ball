﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk membuat toggle gameobject
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalToggle : MonoBehaviour
    {

        public enum CToggleType { ToggleGroup, ToggleEach }
        public CToggleType ToggleType;

        [System.Serializable]
        public class CToggleObject
        {
            public GameObject TargetObject;
            public KeyCode TriggerKey;
        }

        public CToggleObject[] ToggleObject;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            for (int i = 0; i < ToggleObject.Length; i++)
            {
                if (ToggleType == CToggleType.ToggleEach)
                {
                    if (Input.GetKeyUp(ToggleObject[i].TriggerKey))
                    {
                        ToggleObject[i].TargetObject.SetActive(!ToggleObject[i].TargetObject.activeSelf);
                    }
                }
                else if (ToggleType == CToggleType.ToggleGroup)
                {
                    if (Input.GetKeyUp(ToggleObject[i].TriggerKey))
                    {
                        for (int j = 0; j < ToggleObject.Length; j++)
                        {
                            ToggleObject[j].TargetObject.SetActive(false);
                        }
                        ToggleObject[i].TargetObject.SetActive(!ToggleObject[i].TargetObject.activeSelf);
                    }
                }
            }
        }
    }
}
