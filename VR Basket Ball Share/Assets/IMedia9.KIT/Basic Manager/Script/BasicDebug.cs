﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script Hello World Unity. Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu 
 *          dan cuma bisa ngetik dengan jempol. Oh syaaap! 
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDebug : MonoBehaviour {

    public string DebugCaption = "Hello World";

	// Use this for initialization
	void Start () {
        Debug.Log(DebugCaption);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
