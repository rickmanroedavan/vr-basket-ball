﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class Shooter3DMouse : MonoBehaviour
    {
        bool isCooldown = false;
        public enum CForceTrigger { TriggerByIndex, TriggerByKey }
        public CForceTrigger TriggerMode;

        [System.Serializable]
        public class CBullet3D
        {
            public bool isEnabled;
            public KeyCode TriggerKey;
            public bool TriggerTouch;
            public GameObject BulletObject;
            public GameObject BulletPosition;

            [Header("Bullet Speed Settings")]
            public bool usingBulletSpeed;
            public Power3D BulletPower;

            [Header("Projectile Speed Settings")]
            public bool usingProjectileSpeed;
            public Power3D ProjectilePower;

            [Header("Delay Settings")]
            public int ExecuteDelay;
            public int DestroyDelay;
        }

        [Header("Bullet Settings")]
        public int ActiveBulletIndex = 0;
        public CBullet3D[] Bullet3D;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (TriggerMode == CForceTrigger.TriggerByIndex)
            {
                if (Bullet3D[ActiveBulletIndex].isEnabled && !isCooldown)
                {
                    if (Input.GetKeyUp(Bullet3D[ActiveBulletIndex].TriggerKey) || Input.touchCount > 0)
                    {
                        isCooldown = true;
                        Invoke("ExecuteShooter", Bullet3D[ActiveBulletIndex].ExecuteDelay);
                        Invoke("Cooldown", Bullet3D[ActiveBulletIndex].ExecuteDelay);
                    }
                }
            }

            if (TriggerMode == CForceTrigger.TriggerByKey)
            {
                for (int i = 0; i < Bullet3D.Length; i++)
                {
                    if (Bullet3D[i].isEnabled && !isCooldown)
                    {
                        if (Input.GetKeyUp(Bullet3D[i].TriggerKey) || (Bullet3D[i].TriggerTouch && Input.touchCount > 0))
                        {
                            isCooldown = true;
                            ActiveBulletIndex = i;
                            Invoke("ExecuteShooter", Bullet3D[i].ExecuteDelay);
                            Invoke("Cooldown", Bullet3D[i].ExecuteDelay);
                        }
                    }
                }
            }
        }

        void ExecuteShooter()
        {
            GameObject temp = GameObject.Instantiate(Bullet3D[ActiveBulletIndex].BulletObject, Bullet3D[ActiveBulletIndex].BulletPosition.transform.position, Bullet3D[ActiveBulletIndex].BulletPosition.transform.rotation);

            if (Bullet3D[ActiveBulletIndex].usingBulletSpeed)
            {
                temp.GetComponent<Bullet3D>().BulletSpeed = Bullet3D[ActiveBulletIndex].BulletPower.CurrentValue;
            }

            if (Bullet3D[ActiveBulletIndex].usingProjectileSpeed)
            {
                temp.GetComponent<Projectile3D>().BulletSpeed = Bullet3D[ActiveBulletIndex].ProjectilePower.CurrentValue;
            }


            Destroy(temp.gameObject, Bullet3D[ActiveBulletIndex].DestroyDelay);
        }

        void Cooldown()
        {
            isCooldown = false;
        }

        public void SetActiveBulletIndex(int idx)
        {
            ActiveBulletIndex = idx;
        }
    }

}