﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk ngirim info Damage
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class WeaponSender : MonoBehaviour
    {
        public enum COperator { AddValue, SetValue, SubValue }
        public string DamageSenderTag;

        [Header("Calculation Settings")]
        public COperator DamageOperator;
        public float DamageValue;

        [Header("Destroy Settings")]
        public bool DestroyAfterCollision;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}