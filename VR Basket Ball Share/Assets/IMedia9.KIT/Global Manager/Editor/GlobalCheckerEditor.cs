﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script editor untuk checking variabels
 **************************************************************************************************************/

using UnityEditor;
using UnityEngine;

namespace IMedia9
{
    [CustomEditor(typeof(GlobalChecker)), CanEditMultipleObjects]
    public class GlobalCheckerEditor : Editor
    {

        public SerializedProperty
            enabled_prop,
            enum_Status,
            timeVariables_prop,
            healthVariables_prop,
            scoreVariables_prop,
            intVariables_prop,
            floatVariables_prop,
            boolVariables_prop,
            stringVariables_prop,
            ConditionType_prop,
            ConditionValue_prop,
            ConditionEvent_prop
            ;

        void OnEnable()
        {
            // Setup the SerializedProperties
            enabled_prop = serializedObject.FindProperty("isEnabled");
            enum_Status = serializedObject.FindProperty("VariableType");

            timeVariables_prop = serializedObject.FindProperty("TimeVariables");
            healthVariables_prop = serializedObject.FindProperty("HealthVariables");
            scoreVariables_prop = serializedObject.FindProperty("ScoreVariables");
            intVariables_prop = serializedObject.FindProperty("IntVariables");
            floatVariables_prop = serializedObject.FindProperty("FloatVariables");
            boolVariables_prop = serializedObject.FindProperty("BoolVariables");
            stringVariables_prop = serializedObject.FindProperty("StringVariables");

            ConditionType_prop = serializedObject.FindProperty("ConditionType");
            ConditionValue_prop = serializedObject.FindProperty("ConditionValue");
            ConditionEvent_prop = serializedObject.FindProperty("ConditionEvent");

        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(enabled_prop, true);

            EditorGUILayout.PropertyField(enum_Status);

            GlobalVariable.CVariableType st = (GlobalVariable.CVariableType)enum_Status.enumValueIndex;

            switch (st)
            {
                case GlobalVariable.CVariableType.scoreVar:
                    EditorGUILayout.PropertyField(scoreVariables_prop, new GUIContent("ScoreVariables"));
                    break;
                case GlobalVariable.CVariableType.timeVar:
                    EditorGUILayout.PropertyField(timeVariables_prop, new GUIContent("TimeVariables"));
                    break;
                case GlobalVariable.CVariableType.healthVar:
                    EditorGUILayout.PropertyField(healthVariables_prop, new GUIContent("HealthVariables"));
                    break;
                case GlobalVariable.CVariableType.intVar:
                    EditorGUILayout.PropertyField(intVariables_prop, new GUIContent("IntVariables"));
                    break;
                case GlobalVariable.CVariableType.floatVar:
                    EditorGUILayout.PropertyField(floatVariables_prop, new GUIContent("FloatVariables"));
                    break;
                case GlobalVariable.CVariableType.stringVar:
                    EditorGUILayout.PropertyField(stringVariables_prop, new GUIContent("StringVariables"));
                    break;
                case GlobalVariable.CVariableType.boolVar:
                    EditorGUILayout.PropertyField(boolVariables_prop, new GUIContent("BoolVariables"));
                    break;
            }

            EditorGUILayout.PropertyField(ConditionType_prop, true);
            EditorGUILayout.PropertyField(ConditionValue_prop, true);
            EditorGUILayout.PropertyField(ConditionEvent_prop, true);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
