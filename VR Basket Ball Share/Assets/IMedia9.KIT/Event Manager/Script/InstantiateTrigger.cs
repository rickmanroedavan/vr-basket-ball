﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateTrigger : MonoBehaviour {

    public string TriggerTag;
    public GameObject InstantiateObject;
    public GameObject InstantiatePosition;

    [Header("Destroy Setting")]
    public bool usingDelay;
    public int Delay;

    bool isCooldown = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collider)
    {
        if (!isCooldown && collider.tag == TriggerTag)
        {
            isCooldown = true;
            GameObject temp = Instantiate(InstantiateObject, InstantiatePosition.transform.position, InstantiatePosition.transform.rotation);
            if (usingDelay)
            {
                Destroy(temp, Delay);
            }
            Invoke("Cooldown", 1);
        }
    }

    void Cooldown()
    {
        isCooldown = false;
    }
}
