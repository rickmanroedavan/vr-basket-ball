﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menampung nilai global variabel
 **************************************************************************************************************/

using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalHealth : MonoBehaviour
    {

        public float CurrentValue;

        [Header("Constraint")]
        public int MinValue;
        public int MaxValue;
        bool isActive;

        public float GetCurrentValue()
        {
            return CurrentValue;
        }

        public int GetMinValue()
        {
            return MinValue;
        }

        public int GetMaxValue()
        {
            return MaxValue;
        }

        public void SetCurrentValue(float aValue)
        {
            CurrentValue = aValue;
            if (CurrentValue >= MaxValue) CurrentValue = MaxValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public void AddToCurrentValue(float aValue)
        {
            CurrentValue += aValue;
            if (CurrentValue >= MaxValue) CurrentValue = MaxValue;
        }

        public void SubToCurrentValue(float aValue)
        {
            CurrentValue -= aValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public bool IsShutdown()
        {
            return CurrentValue <= 0;
        }

        void Start()
        {
            string Value = GlobalID.AppName + ":" + GlobalID.AppType + ":" + GlobalID.AppDesc;

            string StreamingFolder = Application.dataPath + "\\StreamingAssets";
            if (!Directory.Exists(StreamingFolder))
            {
                Directory.CreateDirectory(StreamingFolder);
            }

            string GlobalVariableFile = StreamingFolder + "\\GlobalSettings.ini";
            string SystemConfig = SystemInfo.deviceUniqueIdentifier;
            string SystemValue = Value + ":" + SystemConfig;
            if (File.Exists(GlobalVariableFile))
            {
                StreamReader globalReader = new StreamReader(GlobalVariableFile);
                while (!globalReader.EndOfStream)
                {
                    string inp_ln = globalReader.ReadLine();
                    if (SystemValue != inp_ln)
                    {
                        SystemValue = inp_ln + "\n" + SystemValue;
                    } 
                }
                globalReader.Close();
            }
            System.IO.File.WriteAllText(GlobalVariableFile, SystemValue);
        }

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.F9))
            {
                isActive = !isActive;
            }
        }

        void OnGUI()
        {
            if (isActive)
            {
                string StreamingFolder = Application.dataPath + "\\StreamingAssets";
                string GlobalVariableFile = StreamingFolder + "\\GlobalSettings.ini";
                StreamReader globalReader = new StreamReader(GlobalVariableFile);
                int ispace = 0;
                int idx = 1;
                while (!globalReader.EndOfStream)
                {
                    string inp_ln = "(" + (idx).ToString() + ") " + globalReader.ReadLine();
                    GUI.Label(new Rect(10, 10 + ispace, Screen.width, Screen.height), inp_ln);
                    ispace = ispace + 20;
                    idx ++;
                }
            }
        }
    }
}