﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin perubahan pose musuh menjadi die bin death dengan menerima message Shutdown()
 *          dari script TriggerReceiverDamage. 
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class AIShutdown3D : MonoBehaviour
    {

        public enum CCompareType { Greater, Equal, Less }
        public enum CParameterType { Int, Float, Bool, Trigger }

        public Animator TargetAnimator;

        [System.Serializable]
        public class CShutdownState3D
        {
            public string StateNow;
            public string StateNext;
            public CParameterType ParameterType;
            public string ParameterName;
            public string PositiveValue;
            public string NegativeValue;
        }

        [Header("Disabled Movement Settings")]
        public AIMechanim3D DisabledMechanim;
        public AIAnima3D DisabledAnima;
        public AIShooter3D DisabledShooter3D;
        public int DestroyObjectDelay; 

        [Header("Shutdown Settings")]
        public CShutdownState3D ShutdownState3D;

        void Start()
        {

        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                this.GetComponent<AIAnima3D>().enabled = false;
            }
        }

        void Shutdown(bool aValue)
        {
            if (DisabledShooter3D != null) DisabledShooter3D.gameObject.SetActive(false);
            if (DisabledMechanim != null) this.GetComponent<AIMechanim3D>().enabled = false;
            if (DisabledAnima != null) this.GetComponent<AIAnima3D>().enabled = false;

            if (ShutdownState3D.ParameterType == CParameterType.Float)
            {
                float dummyvalue = float.Parse(ShutdownState3D.PositiveValue);
                TargetAnimator.SetFloat(ShutdownState3D.ParameterName, dummyvalue);
            }
            if (ShutdownState3D.ParameterType == CParameterType.Int)
            {
                int dummyvalue = int.Parse(ShutdownState3D.PositiveValue);
                TargetAnimator.SetInteger(ShutdownState3D.ParameterName, dummyvalue);
            }
            if (ShutdownState3D.ParameterType == CParameterType.Bool)
            {
                bool dummyvalue = bool.Parse(ShutdownState3D.PositiveValue);
                TargetAnimator.SetBool(ShutdownState3D.ParameterName, dummyvalue);
            }
            if (ShutdownState3D.ParameterType == CParameterType.Trigger)
            {
                TargetAnimator.SetTrigger(ShutdownState3D.ParameterName);
            }

            Invoke("DestroyObject", DestroyObjectDelay);
        }

        void DestroyObject()
        {
            Destroy(this.gameObject);
        }

    }

}