﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script editor untuk body
 **************************************************************************************************************/

using UnityEditor;
using UnityEngine;

namespace IMedia9
{
    [CustomEditor(typeof(GlobalCaption)), CanEditMultipleObjects]
    public class GlobalCaptionEditor : Editor
    {

        public SerializedProperty
            enum_Status,
            enabled_prop,
            timeVariables_prop,
            healthVariables_prop,
            scoreVariables_prop,
            intVariables_prop,
            floatVariables_prop,
            boolVariables_prop,
            stringVariables_prop,
            usingDebug_prop,
            DebugText_prop,
            usingUI_prop,
            CaptionText_prop,
            pCaptionText_prop,
            usingTime_prop,
            TimeFormat_prop,
            CaptionTime_prop,
            pCaptionTime_prop
            ;

        void OnEnable()
        {
            // Setup the SerializedProperties
            enum_Status = serializedObject.FindProperty("VariableType");

            enabled_prop = serializedObject.FindProperty("isEnabled");
            timeVariables_prop = serializedObject.FindProperty("TimeVariables");
            healthVariables_prop = serializedObject.FindProperty("HealthVariables");
            scoreVariables_prop = serializedObject.FindProperty("ScoreVariables");
            intVariables_prop = serializedObject.FindProperty("IntVariables");
            floatVariables_prop = serializedObject.FindProperty("FloatVariables");
            boolVariables_prop = serializedObject.FindProperty("BoolVariables");
            stringVariables_prop = serializedObject.FindProperty("StringVariables");
            usingDebug_prop = serializedObject.FindProperty("usingDebug");
            DebugText_prop = serializedObject.FindProperty("DebugText");
            usingUI_prop = serializedObject.FindProperty("usingUI");
            CaptionText_prop = serializedObject.FindProperty("CaptionText");
            pCaptionText_prop = serializedObject.FindProperty("PrefixText");
            usingTime_prop = serializedObject.FindProperty("usingTimeFormat");
            TimeFormat_prop = serializedObject.FindProperty("TimeFormat");
            CaptionTime_prop = serializedObject.FindProperty("CaptionTime");
            pCaptionTime_prop = serializedObject.FindProperty("PrefixTime");

        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(enabled_prop, true);

            EditorGUILayout.PropertyField(enum_Status);

            GlobalVariable.CVariableType st = (GlobalVariable.CVariableType)enum_Status.enumValueIndex;

            switch (st)
            {
                case GlobalVariable.CVariableType.scoreVar:
                    EditorGUILayout.PropertyField(scoreVariables_prop, new GUIContent("ScoreVariables"));
                    break;
                case GlobalVariable.CVariableType.timeVar:
                    EditorGUILayout.PropertyField(timeVariables_prop, new GUIContent("TimeVariables"));

                    EditorGUILayout.PropertyField(usingTime_prop, true);
                    EditorGUILayout.PropertyField(TimeFormat_prop, true);
                    EditorGUILayout.PropertyField(CaptionTime_prop, true);
                    EditorGUILayout.PropertyField(pCaptionTime_prop, true);

                    break;
                case GlobalVariable.CVariableType.healthVar:
                    EditorGUILayout.PropertyField(healthVariables_prop, new GUIContent("HealthVariables"));
                    break;
                case GlobalVariable.CVariableType.intVar:
                    EditorGUILayout.PropertyField(intVariables_prop, new GUIContent("IntVariables"));
                    break;
                case GlobalVariable.CVariableType.floatVar:
                    EditorGUILayout.PropertyField(floatVariables_prop, new GUIContent("FloatVariables"));
                    break;
                case GlobalVariable.CVariableType.stringVar:
                    EditorGUILayout.PropertyField(stringVariables_prop, new GUIContent("StringVariables"));
                    break;
                case GlobalVariable.CVariableType.boolVar:
                    EditorGUILayout.PropertyField(boolVariables_prop, new GUIContent("BoolVariables"));
                    break;
            }

            EditorGUILayout.PropertyField(usingUI_prop, true);
            EditorGUILayout.PropertyField(CaptionText_prop, true);
            EditorGUILayout.PropertyField(pCaptionText_prop, true);

            EditorGUILayout.PropertyField(usingDebug_prop, true);
            EditorGUILayout.PropertyField(DebugText_prop, true);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
