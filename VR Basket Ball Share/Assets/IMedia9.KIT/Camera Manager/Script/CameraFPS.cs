﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera untuk pembuatan game fps, khususnya pada pergerakan 
 *          rotasi mouse dengan menjadikan senjata player sebagai childnya.  
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class CameraFPS : MonoBehaviour
    {
        public bool isEnabled;
        public Camera TargetCamera;
        public GameObject TargetObject;

        [System.Serializable]
        public class CMouseController
        {
            public bool isEnabled;
            public bool isAutoRotate;
            public bool isCursorVisible;
            public KeyCode MouseLookButton = KeyCode.Mouse1;
        }

        [Header("Mouse Settings")]
        public CMouseController MouseController;
        public float LookSpeed = 4f;

        int mousePanButton;
        Vector3 startPosition;
        Quaternion startRotation;
        Vector3 lastPosition;
        Vector3 deltaPosition;

        Vector3 targetPosition;
        Quaternion targetRotation;
        float targetRotationY;
        float targetRotationX;

        void SyncronizeInput()
        {
            TargetCamera.transform.position = TargetObject.transform.position - deltaPosition;

            TargetObject.transform.rotation = TargetCamera.transform.rotation;

            targetPosition = TargetCamera.transform.position;
            targetRotation = TargetCamera.transform.rotation;
            targetRotationY = TargetCamera.transform.localRotation.eulerAngles.y;
            targetRotationX = TargetCamera.transform.localRotation.eulerAngles.x;

            if (Input.GetKey(KeyCode.Escape))
            {
                Cursor.visible = !Cursor.visible;
            }
        }

        // Use this for initialization
        void Start()
        {
            startPosition = TargetCamera.transform.position;
            startRotation = TargetCamera.transform.rotation;
            deltaPosition = TargetObject.transform.position - TargetCamera.transform.position;

            Cursor.visible = MouseController.isCursorVisible;

            SyncronizeInput();
        }

        // Update is called once per frame
        void Update()
        {
            if (isEnabled)
            {

                //-- Mouse Controller
                if (MouseController.isEnabled)
                {
                    if (MouseController.isAutoRotate || Input.GetKey(MouseController.MouseLookButton))
                    {
                        targetRotationY += Input.GetAxis("Mouse X") * LookSpeed;
                        targetRotationX -= Input.GetAxis("Mouse Y") * LookSpeed;
                        targetRotation = Quaternion.Euler(targetRotationX, targetRotationY, 0.0f);
                        TargetCamera.transform.rotation = targetRotation;
                    }
                }
            }
        }

        void LateUpdate()
        {
            SyncronizeInput();
        }

        public void SetEnabled(bool aValue)
        {
            isEnabled = aValue;
        }
    }
}

