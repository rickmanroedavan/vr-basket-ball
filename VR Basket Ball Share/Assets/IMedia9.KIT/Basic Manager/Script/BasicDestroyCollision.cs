﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script yang menunjukkan dasar-dasar fungsi di Unity khususnya pada Collision
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu 
 *          dan cuma bisa ngetik dengan jempol. Oh syaaap! 
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDestroyCollision : MonoBehaviour {

    public enum CTargetType { ThisGameObject, ThatGameObject }
    public CTargetType TargetType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        if (TargetType == CTargetType.ThisGameObject)
        {
            Destroy(this.gameObject);
        } else if (TargetType == CTargetType.ThatGameObject)
        {
            Destroy(collision.gameObject);
        }
    }
}
