﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace IMedia9
{
    public class GlobalMouse : MonoBehaviour
    {
        public Camera PrimaryCamera;
        public Camera SecondaryCamera;
        public bool isAuto;

        public static float MouseX;
        public static float MouseY;
        public static float CoordX;
        public static float CoordY;
        public static float CoordZ;
        public static Vector3 VectorMouse;

        public enum CMouseType { MouseDefault, MousePlacement, MouseSelect, MouseSelectZoom, MouseTranslate, MouseMap, MousePan }

        [Header("Mouse Status")]
        public static CMouseType MouseType;

        [Header("Debug Value")]
        public bool usingDebug;
        public float debugMouseX;
        public float debugMouseY;
        public float debugCoordX;
        public float debugCoordY;
        public float debugCoordZ;

        Ray ray; 
        RaycastHit raycastHit;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (isAuto)
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraPrimary)
                    {
                        ray = PrimaryCamera.ScreenPointToRay(Input.mousePosition);
                    } else
                    if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraSecondary)
                    {
                        ray = SecondaryCamera.ScreenPointToRay(Input.mousePosition);
                    }

                    MouseX = Input.mousePosition.x;
                    MouseY = Input.mousePosition.y;
                    if (Physics.Raycast(ray, out raycastHit))
                    {
                        CoordX = raycastHit.point.x;
                        CoordY = raycastHit.point.y;
                        CoordZ = raycastHit.point.z;
                        VectorMouse = raycastHit.point;
                    }

                    if (usingDebug)
                    {
                        debugMouseX = MouseX;
                        debugMouseY = MouseY;
                        debugCoordX = CoordX;
                        debugCoordY = CoordY;
                        debugCoordZ = CoordZ;
                    }
                }
            } else if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraPrimary)
                    {
                        ray = PrimaryCamera.ScreenPointToRay(Input.mousePosition);
                    }
                    else
                    if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraSecondary)
                    {
                        ray = SecondaryCamera.ScreenPointToRay(Input.mousePosition);
                    }

                    MouseX = Input.mousePosition.x;
                    MouseY = Input.mousePosition.y;
                    if (Physics.Raycast(ray, out raycastHit))
                    {
                        CoordX = raycastHit.point.x;
                        CoordY = raycastHit.point.y;
                        CoordZ = raycastHit.point.z;
                        VectorMouse = raycastHit.point;
                    }

                    if (usingDebug)
                    {
                        debugMouseX = MouseX;
                        debugMouseY = MouseY;
                        debugCoordX = CoordX;
                        debugCoordY = CoordY;
                        debugCoordZ = CoordZ;
                    }
                }
            }
        }
		
		public static Vector3 GetVectorCoordXYZ(){
			return new Vector3(CoordX, CoordY, CoordZ);
		}

        public static string GetStringMouseXY()
        {
            return "MouseXY: " + Mathf.RoundToInt(MouseX).ToString() + "," + Mathf.RoundToInt(MouseY).ToString();
        }

        public static string GetStringCoordXYZ()
        {
            return "CoordXYZ: " + CoordX.ToString() + "," + CoordY.ToString() + "," + CoordZ.ToString();
        }

        public void ChangeMouseStatus(int aValue)
        {
            switch (aValue)
            {
                case 1:
                    MouseType = CMouseType.MouseDefault;
                    //UnitConsole.PrintDebug("GlobalMouse (132): Mouse Active: Default");
                    break;
                case 2:
                    MouseType = CMouseType.MousePlacement;
                    //UnitConsole.PrintDebug("GlobalMouse (148): Mouse Active: Placement");
                    break;
                case 3:
                    MouseType = CMouseType.MouseSelect;
                    //UnitConsole.PrintDebug("GlobalMouse (136): Mouse Active: Select");
                    break;
                case 4:
                    MouseType = CMouseType.MouseSelectZoom;
                    //UnitConsole.PrintDebug("GlobalMouse (140): Mouse Active: Select Zoom");
                    break;
                case 5:
                    MouseType = CMouseType.MouseTranslate;
                    //UnitConsole.PrintDebug("GlobalMouse (152): Mouse Active: Translate");
                    break;
                case 6:
                    MouseType = CMouseType.MouseMap;
                    //UnitConsole.PrintDebug("GlobalMouse (144): Mouse Active: Map");
                    break;
                case 7:
                    MouseType = CMouseType.MousePan;
                    //UnitConsole.PrintDebug("GlobalMouse (156): Mouse Active: Pan");
                    break;
            }
        }

    }
}

