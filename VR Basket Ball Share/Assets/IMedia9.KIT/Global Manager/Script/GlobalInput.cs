﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script editor untuk global input
 **************************************************************************************************************/

 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace IMedia9
{
    public class GlobalInput : MonoBehaviour
    {

        public bool isEnabled;

        [Header("KeyDown Action Settings")]
        public KeyCode TriggerKeyDown;
        public UnityEvent KeyDownCondition;

        [Header("KeyPress Action Settings")]
        public KeyCode TriggerKeyPress;
        public UnityEvent KeyPressCondition;

        [Header("KeyUp Action Settings")]
        public KeyCode TriggerKeyUp;
        public UnityEvent KeyUpCondition;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(TriggerKeyDown))
            {
                KeyDownCondition.Invoke();
            }
            if (Input.GetKey(TriggerKeyPress))
            {
                KeyPressCondition.Invoke();
            }
            if (Input.GetKeyUp(TriggerKeyUp))
            {
                KeyUpCondition.Invoke();
            }
        }
    }
}
