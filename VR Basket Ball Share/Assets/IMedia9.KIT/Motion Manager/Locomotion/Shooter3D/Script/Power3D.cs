﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Power3D : MonoBehaviour
{
    public Slider MainSlider;

    [Header("Increment Value")]
    public float CurrentValue;
    public int MinValue = 0;
    public int MaxValue = 100;

    [Header("Interval Value")]
    public float Interval = 0.1f;
    public float Increment = 1f;

    bool statusGauge = false;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("RepeatFunction", 1, Interval);
    }

    void RepeatFunction()
    {
        if (!statusGauge)
        {
            if (CurrentValue < MaxValue)
            {
                CurrentValue += Increment;
            }
            else
            {
                CurrentValue = MaxValue;
                statusGauge = true;
            }
        }
        if (statusGauge)
        {
            if (CurrentValue > MinValue)
            {
                CurrentValue -= Increment;
            }
            else
            {
                CurrentValue = MinValue;
                statusGauge = false;
            }
        }
        MainSlider.value = (CurrentValue/MaxValue) * 100;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
