﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera ala RPG third person, dengan mouse bisa ngelilingin si player
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CameraRPG : MonoBehaviour
    {
        public bool isEnabled;
        public Camera TargetCamera;
        public GameObject TargetObject;
        public float FarDistance = 4f;
        public float UpDistance = 4f;

        [System.Serializable]
        public class CMouseController
        {
            public bool isEnabled;
            public bool isAutoRotate;
            [Header("Primary Button")]
            public KeyCode MouseLookButton = KeyCode.Mouse1;
            public KeyCode MouseScrollButton = KeyCode.Mouse2;
        }

        [Header("Mouse Settings")]
        public CMouseController MouseController;

        [Header("Special Settings")]
        public float Sensitivity = 15f;
        public float PitchMin = 0;
        public float PitchMax = 60;
        public float FarDelta = 0.1f;
        public float FarDistanceMin = 0;
        public float FarDistanceMax = 4;
        float Pitch = 0;
        float Yaw = 0;
        float Roll = 0;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (isEnabled)
            {
                Pitch -= Sensitivity * Input.GetAxis("Mouse Y");
                Yaw += Sensitivity * Input.GetAxis("Mouse X");

                Pitch = Mathf.Clamp(Pitch, PitchMin, PitchMax);

                if (MouseController.MouseScrollButton == KeyCode.Mouse2 && Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    FarDistance += FarDelta;
                    FarDistance = Mathf.Clamp(FarDistance, FarDistanceMin, FarDistanceMax);
                }
                if (MouseController.MouseScrollButton == KeyCode.Mouse2 && Input.GetAxis("Mouse ScrollWheel") < 0)
                {
                    FarDistance -= FarDelta;
                    FarDistance = Mathf.Clamp(FarDistance, FarDistanceMin, FarDistanceMax);
                }

                if (MouseController.isAutoRotate)
                {
                    TargetCamera.transform.eulerAngles = new Vector3(Pitch, Yaw, Roll);
                } else if (Input.GetKey(MouseController.MouseLookButton))
                {
                    TargetCamera.transform.eulerAngles = new Vector3(Pitch, Yaw, Roll);
                }

                TargetCamera.transform.position = TargetObject.transform.position - FarDistance * TargetCamera.transform.forward + Vector3.up * UpDistance;
            }
        }
    }
}
