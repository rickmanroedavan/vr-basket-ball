﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera secara bebas, lepas, tanpa beban yang ada seperti burung-burung
 *          terbang di udara, oh syaaap! Dibuat untuk mendeteksi keyboard, joystick, dan mouse. 
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class CameraUFO : MonoBehaviour
    {
        public bool isEnabled;
        public Camera TargetCamera;

        [System.Serializable]
        public class CKeyboardController
        {
            public bool isEnabled;
            [Header("Primary Keys")]
            public KeyCode ForwardKey = KeyCode.UpArrow;
            public KeyCode LeftKey = KeyCode.LeftArrow;
            public KeyCode BackwardKey = KeyCode.DownArrow;
            public KeyCode RightKey = KeyCode.RightArrow;
            public KeyCode UpKey = KeyCode.PageUp;
            public KeyCode DownKey = KeyCode.PageDown;
            public KeyCode PitchUp = KeyCode.Insert;
            public KeyCode PitchDown = KeyCode.Delete;
            public KeyCode YawLeft = KeyCode.Home;
            public KeyCode YawRight = KeyCode.End;
            public KeyCode RollLeft = KeyCode.Home;
            public KeyCode RollRight = KeyCode.End;
            public KeyCode JumpKey = KeyCode.None;
            public KeyCode ShiftKey = KeyCode.None;

            [Header("Alternative Keys")]
            public KeyCode AltForwardKey = KeyCode.W;
            public KeyCode AltLeftKey = KeyCode.A;
            public KeyCode AltBackwardKey = KeyCode.S;
            public KeyCode AltRightKey = KeyCode.D;
            public KeyCode AltUpKey = KeyCode.None;
            public KeyCode AltDownKey = KeyCode.None;
            public KeyCode AltPitchUp = KeyCode.None;
            public KeyCode AltPitchDown = KeyCode.None;
            public KeyCode AltYawLeft = KeyCode.None;
            public KeyCode AltYawRight = KeyCode.None;
            public KeyCode AltRollLeft = KeyCode.None;
            public KeyCode AltRollRight = KeyCode.None;
            public KeyCode AltJumpKey = KeyCode.None;
            public KeyCode AltShiftKey = KeyCode.None;
        }

        [System.Serializable]
        public class CJoystickController
        {
            public bool isEnabled;
            [Header("Primary Pad")]
            public KeyCode ForwardKey = KeyCode.Joystick1Button0;
            public KeyCode LeftKey = KeyCode.Joystick1Button1;
            public KeyCode BackwardKey = KeyCode.Joystick1Button2;
            public KeyCode RightKey = KeyCode.Joystick1Button3;
            public KeyCode UpKey = KeyCode.Joystick1Button4;
            public KeyCode DownKey = KeyCode.Joystick1Button5;
            public KeyCode YawLeft = KeyCode.Joystick1Button6;
            public KeyCode YawRight = KeyCode.Joystick1Button7;
            public KeyCode PitchUp = KeyCode.Joystick1Button8;
            public KeyCode PitchDown = KeyCode.Joystick1Button9;
            public KeyCode RollLeft = KeyCode.Joystick1Button10;
            public KeyCode RollRight = KeyCode.Joystick1Button11;
            public KeyCode JumpKey = KeyCode.None;
            public KeyCode ShiftKey = KeyCode.None;
        }

        [System.Serializable]
        public class CMouseController
        {
            public bool isEnabled;
            public bool isAutoRotate;
            [Header("Primary Button")]
            public KeyCode MouseLookButton = KeyCode.Mouse1;
            public KeyCode MousePanButton = KeyCode.Mouse2;
            public KeyCode MouseScrollButton = KeyCode.Mouse2;
        }

        [Header("Keyboard Settings")]
        public CKeyboardController KeyboardController;
        [Header("Joystick Settings")]
        public CJoystickController JoystickController;
        [Header("Mouse Settings")]
        public CMouseController MouseController;

        [Header("Special Settings")]
        public KeyCode ResetKey = KeyCode.F5;
        public float MoveSpeed = 100;
        public float ShiftSpeed = 10;
        public float PitchSpeed = 1;
        public float YawSpeed = 1;
        public float RollSpeed = 1;
        public float ScrollSpeed = 0.1f;
        public float LookSpeed = 4f;
        public float PanSpeed = 1.0f;
        float NormalSpeed;

        int mousePanButton;
        Vector3 startPosition;
        Quaternion startRotation;
        Vector3 lastPosition;

        Vector3 targetPosition;
        Quaternion targetRotation;
        float targetRotationY;
        float targetRotationX;

        void SyncronizeInput()
        {
            targetPosition = TargetCamera.transform.position;
            targetRotation = TargetCamera.transform.rotation;
            targetRotationY = TargetCamera.transform.localRotation.eulerAngles.y;
            targetRotationX = TargetCamera.transform.localRotation.eulerAngles.x;
        }

        // Use this for initialization
        void Start()
        {
            NormalSpeed = MoveSpeed;

            startPosition = TargetCamera.transform.position;
            startRotation = TargetCamera.transform.rotation;

            if (MouseController.MousePanButton == KeyCode.Mouse0) mousePanButton = 0;
            if (MouseController.MousePanButton == KeyCode.Mouse1) mousePanButton = 1;
            if (MouseController.MousePanButton == KeyCode.Mouse2) mousePanButton = 2;

            SyncronizeInput();
        }

        // Update is called once per frame
        void Update()
        {
            if (isEnabled)
            {
                if (Input.GetKey(ResetKey))
                {
                    TargetCamera.transform.position = startPosition;
                    TargetCamera.transform.rotation = startRotation;
                }

                //-- Keyboard Controller
                if (KeyboardController.isEnabled)
                {
                    if (Input.GetKeyDown(KeyCode.LeftShift))
                    {
                        MoveSpeed = ShiftSpeed;
                    }
                    if (Input.GetKeyUp(KeyCode.LeftShift))
                    {
                        MoveSpeed = NormalSpeed;
                    }

                    if (Input.GetKey(KeyboardController.ForwardKey) || Input.GetKey(KeyboardController.AltForwardKey))
                    {
                        TargetCamera.transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.BackwardKey) || Input.GetKey(KeyboardController.AltBackwardKey))
                    {
                        TargetCamera.transform.Translate(Vector3.back * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.LeftKey) || Input.GetKey(KeyboardController.AltLeftKey))
                    {
                        TargetCamera.transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.RightKey) || Input.GetKey(KeyboardController.AltRightKey))
                    {
                        TargetCamera.transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.UpKey) || Input.GetKey(KeyboardController.AltUpKey))
                    {
                        TargetCamera.transform.Translate(Vector3.up * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.DownKey) || Input.GetKey(KeyboardController.DownKey))
                    {
                        TargetCamera.transform.Translate(Vector3.down * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.YawLeft) || Input.GetKey(KeyboardController.AltYawLeft))
                    {
                        TargetCamera.transform.Rotate(0, YawSpeed, 0);
                    }
                    if (Input.GetKey(KeyboardController.YawRight) || Input.GetKey(KeyboardController.AltYawRight))
                    {
                        TargetCamera.transform.Rotate(0, -YawSpeed, 0);
                    }
                    if (Input.GetKey(KeyboardController.PitchUp) || Input.GetKey(KeyboardController.AltPitchUp))
                    {
                        TargetCamera.transform.Rotate(-PitchSpeed, 0, 0);
                    }
                    if (Input.GetKey(KeyboardController.PitchDown) || Input.GetKey(KeyboardController.AltPitchDown))
                    {
                        TargetCamera.transform.Rotate(PitchSpeed, 0, 0);
                    }
                    if (Input.GetKey(KeyboardController.RollLeft) || Input.GetKey(KeyboardController.AltRollLeft))
                    {
                        TargetCamera.transform.Rotate(0, 0, RollSpeed);
                    }
                    if (Input.GetKey(KeyboardController.RollRight) || Input.GetKey(KeyboardController.AltRollRight))
                    {
                        TargetCamera.transform.Rotate(0, 0, -RollSpeed);
                    }
                }


                //-- Joystick Controller
                if (JoystickController.isEnabled)
                {
                    if (Input.GetKey(JoystickController.ForwardKey))
                    {
                        TargetCamera.transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(JoystickController.BackwardKey))
                    {
                        TargetCamera.transform.Translate(Vector3.back * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(JoystickController.LeftKey))
                    {
                        TargetCamera.transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(JoystickController.RightKey))
                    {
                        TargetCamera.transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(JoystickController.UpKey))
                    {
                        TargetCamera.transform.Translate(Vector3.up * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(JoystickController.DownKey))
                    {
                        TargetCamera.transform.Translate(Vector3.down * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(JoystickController.YawLeft))
                    {
                        TargetCamera.transform.Rotate(0, YawSpeed, 0);
                    }
                    if (Input.GetKey(JoystickController.YawRight))
                    {
                        TargetCamera.transform.Rotate(0, -YawSpeed, 0);
                    }
                    if (Input.GetKey(JoystickController.PitchUp))
                    {
                        TargetCamera.transform.Rotate(-PitchSpeed, 0, 0);
                    }
                    if (Input.GetKey(JoystickController.PitchDown))
                    {
                        TargetCamera.transform.Rotate(PitchSpeed, 0, 0);
                    }
                    if (Input.GetKey(JoystickController.RollLeft))
                    {
                        TargetCamera.transform.Rotate(0, 0, RollSpeed);
                    }
                    if (Input.GetKey(JoystickController.RollRight))
                    {
                        TargetCamera.transform.Rotate(0, 0, -RollSpeed);
                    }
                }

                //-- Mouse Controller
                if (MouseController.isEnabled)
                {
                    if (MouseController.isAutoRotate || Input.GetKey(MouseController.MouseLookButton))
                    {
                        targetRotationY += Input.GetAxis("Mouse X") * LookSpeed;
                        targetRotationX -= Input.GetAxis("Mouse Y") * LookSpeed;
                        targetRotation = Quaternion.Euler(targetRotationX, targetRotationY, 0.0f);
                        TargetCamera.transform.rotation = targetRotation;
                    }

                    if (MouseController.MouseScrollButton == KeyCode.Mouse2 && Input.GetAxis("Mouse ScrollWheel") > 0)
                    {
                        TargetCamera.transform.Translate(Vector3.forward * ScrollSpeed * Time.deltaTime);
                    }
                    if (MouseController.MouseScrollButton == KeyCode.Mouse2 && Input.GetAxis("Mouse ScrollWheel") < 0)
                    {
                        TargetCamera.transform.Translate(Vector3.back * ScrollSpeed * Time.deltaTime);
                    }

                    if (Input.GetMouseButtonDown(mousePanButton))
                    {
                        lastPosition = Input.mousePosition;
                    }
                    if (Input.GetMouseButton(mousePanButton))
                    {
                        Vector3 delta = Input.mousePosition - lastPosition;
                        TargetCamera.transform.Translate(-delta.x * PanSpeed, -delta.y * PanSpeed, 0);
                        lastPosition = Input.mousePosition;
                        targetPosition = TargetCamera.transform.position;
                    }
                }
            }
        }

        void LateUpdate()
        {
            SyncronizeInput();
        }

        public void ActivateCamera(Toggle ToggleMap)
        {
            ToggleMap.isOn = true;
            KeyboardController.isEnabled = ToggleMap.isOn;
            MouseController.isEnabled = ToggleMap.isOn;
        }

        public void ActivateCamera(bool aValue)
        {
            KeyboardController.isEnabled = aValue;
            MouseController.isEnabled = aValue;
        }

        public void InActivateCamera(Toggle ToggleMap)
        {
            ToggleMap.isOn = false;
            KeyboardController.isEnabled = ToggleMap.isOn;
            MouseController.isEnabled = ToggleMap.isOn;
        }

        public void ChangeCameraStatus(Toggle ToggleMap)
        {
            KeyboardController.isEnabled = ToggleMap.isOn;
            MouseController.isEnabled = ToggleMap.isOn;
        }

    }
}

