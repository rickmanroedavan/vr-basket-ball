﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace IMedia9
{

    public class LevelManager : MonoBehaviour
    {

        public enum CLoadingType { ClickOnly, ClickAndLoading, DelayOnly, DelayAndLoading, CollisionOnly, CollisionAndLoading }
        public enum CCollisionType { OnCollision, OnTrigger }
        public CLoadingType LoadingType;
        public string NextSceneName;

        [Header("Delay Settings")]
        public int WaitSecond = 10;
        public bool WithLoadingScreen;
        public int NextSceneIndex;

        [Header("Collision Settings")]
        public CCollisionType CollisionType;
        public string ObjectTag;
        public static string GlobalLoadingScene = "GlobalLoadingScreen";

        // Use this for initialization
        void Start()
        {
            if (LoadingType == CLoadingType.DelayOnly)
            {
                WithLoadingScreen = false;
                Invoke("NextSceneWithDelay", WaitSecond);
            } 
            if (LoadingType == CLoadingType.DelayAndLoading)
            {
                WithLoadingScreen = true;
                Invoke("NextSceneWithDelay", WaitSecond);
            }
            if (LoadingType == CLoadingType.ClickOnly)
            {
                WithLoadingScreen = false;
            }
            if (LoadingType == CLoadingType.ClickAndLoading)
            {
                WithLoadingScreen = true;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ClickOnlyGoToScene(string SceneName)
        {
            SceneManager.LoadScene(SceneName);
        }

        public void ClickAndLoadingGoToScene(int SceneIndex)
        {
            PlayerPrefs.SetInt(LoadingManager.NextSceneIndex, SceneIndex);
            Debug.Log("Save NextScene Index: " + SceneIndex.ToString());
            LevelManager.LoadScene(SceneIndex);
        }


        void NextSceneWithDelay()
        {
            if (WithLoadingScreen)
            {
                LevelManager.LoadScene(NextSceneIndex);
            }
            else
            {
                SceneManager.LoadScene(NextSceneName);

            }
        }

        void NextSceneWithClick()
        {
            if (WithLoadingScreen)
            {
                LevelManager.LoadScene(NextSceneIndex);
            }
            else
            {
                SceneManager.LoadScene(NextSceneName);
            }
        }

        void NextSceneClickOnly()
        {
            if (WithLoadingScreen)
            {
                LevelManager.LoadScene(NextSceneIndex);
            }
            else
            {
                SceneManager.LoadScene(NextSceneName);
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            if (LoadingType == CLoadingType.CollisionAndLoading)
            {
                WithLoadingScreen = true;
            }

            if (LoadingType == CLoadingType.CollisionOnly || LoadingType == CLoadingType.CollisionAndLoading)
            {
                if (CollisionType == CCollisionType.OnTrigger)
                {
                    if (collider.gameObject.tag == ObjectTag)
                    {
                        if (WithLoadingScreen)
                        {
                            LevelManager.LoadScene(NextSceneIndex);
                        }
                        else
                        {
                            SceneManager.LoadScene(NextSceneName);
                        }

                    }
                }
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (LoadingType == CLoadingType.CollisionAndLoading)
            {
                WithLoadingScreen = true;
            }

            if (LoadingType == CLoadingType.CollisionOnly || LoadingType == CLoadingType.CollisionAndLoading)
            {
                if (CollisionType == CCollisionType.OnCollision)
                {
                    if (collision.gameObject.tag == ObjectTag)
                    {
                        if (WithLoadingScreen)
                        {
                            LevelManager.LoadScene(NextSceneIndex);
                        }
                        else
                        {
                            SceneManager.LoadScene(NextSceneName);
                        }
                    }
                }
            }
        }

        public static void LoadScene(int scene_index)
        {
            PlayerPrefs.SetInt(LoadingManager.NextSceneIndex, scene_index);
            Debug.Log("Save NextScene Index: " + scene_index.ToString());
            SceneManager.LoadScene(GlobalLoadingScene);
        }

    }

}